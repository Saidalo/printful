<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ToDoLists extends Model
{
    protected $fillable = ['done', 'task-text', 'list_id'];
    use HasFactory;

    public function post() {
        return $this->belongsTo(Lists::class);
    }
}
