<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{
    protected $fillable = ['listName', 'label'];
    use HasFactory;

    public function toDoList() {
        return $this->hasMany(ToDoList::class);
    }
}
