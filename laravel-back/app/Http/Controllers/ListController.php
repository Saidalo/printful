<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lists;
use App\Models\ToDoLists;

class ListController extends Controller
{
    public function index(Request $request)
    {
        $sort = $request->input('sort');
        if ($sort && $sort != '0') {
            return Lists::orderBy('created_at', $sort)->get();
        }
        
        return Lists::all();
    }

    public function show(Request $request, $id)
    {
        $list = Lists::find($id);
        $sort = $request->input('sort');
        $toDoList = ToDoLists::where('list_id', $id);
        if ($sort && $sort != '0') {
            $toDoList = $toDoList->orderBy('done', $sort);
        }
        $list['toDoList'] = $toDoList->get(); 
        return $list;
    }

    public function store(Request $request)
    {
        $listName = $request->json('listName');
        $label = $request->json('label');
        $lists = Lists::create([
            'listName' => $listName,
            'label' => $label
        ]);

        return response()->json($lists, 201);
    }

    public function saveList(Request $request) {
        $listId = $request->json('listId');
        $toDoList = $request->json('toDoList');
        $listName = $request->json('listname');
        $list = Lists::find($listId);
        $list->update(['listName' => $listName]);
        $toDoList = array_map(function($list) use ($listId){
            $data['list_id'] = $listId;
            $data['done'] = $list['done'];
            $data['task-text'] = $list['task-text'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            return $data;
        }, $toDoList);

        ToDoLists::where('list_id', $listId)->delete();
        ToDoLists::insert($toDoList);
        return response()->json(['aa' => $toDoList]);
    }

    public function setStatus($id, Request $request) {
        $list = ToDoLists::find($request->json('listId'));
        $list->update(['done' => (bool)!$request->json('status')]);
        
        return response()->json(['status' => 'success']);
    }

    public function delete($id) {
        Lists::find($id)->delete();
        return response()->json(['status' => 'success']);
    }
}
