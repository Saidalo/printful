<?php

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('lists', 'ListController@index');
Route::get('list/{id}', 'ListController@show');
Route::post('lists', 'ListController@store');
Route::post('list-edit', 'ListController@saveList');
Route::put('lists/{id}', 'ListController@update');
Route::put('lists/{id}/set-status', 'ListController@setStatus');
Route::delete('lists/{id}', 'ListController@delete');