import { createWebHistory, createRouter } from "vue-router"
import IndexComponent from "../views/index.vue"
import CreateList from "../views/CreateList.vue"
import GetList from "../views/GetList.vue"
import EditList from "../views/EditList.vue"

const routes = [
  {
    path: "/",
    name: "index",
    component: IndexComponent,
  },
  {
    path: "/create",
    name: "Create",
    component: CreateList,
  },
  {
    path: "/list/:listId",
    name: "GetList",
    component: GetList,
  },
  {
    path: "/list-edit/:listId",
    name: "EditList",
    component: EditList,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;