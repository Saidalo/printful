import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import IndexComponent from "@/views/index.vue"
import axios from 'axios'
import VueAxios from 'vue-axios'

const app = createApp(App).use(router)
app.use(VueAxios, axios)
app.component('index', IndexComponent)
app.mount('#app')